# [Golang][Web] Layout in http/template

A tiny demo app for layout in [http/template](https://golang.org/pkg/html/template/).

## Usage

Use [Git](https://git-scm.com/) to clone this repo:

```
$ git clone https://gitlab.com/cwchen/GolangWebTemplateLayout.git
```

Run this program:

```
$ cd GolangWebTemplateLayout
$ go run main.go
```

Visit http://localhost:8080 for the result:

![Layout in http/template](images/golang-web-template-layout.PNG)

## Copyright

2018, Michael Chen; Apache 2.0.
